# 试问 - LLM 应用开发工具集

## 与广大的开发者共建
 - 开源。
 - 本地运行。
 - 多种API支持，线上Api和本地Ollama。
 - 兼容多种LLM，商用的和开源的。
 - Spring boot和Vue。
## 功能全面
 - Prompt调试矩阵。
 - Prompt档案馆。
 - 向量数据库数据管理。
 - 文档灌库工具。
 - FunctionCalling调试。
 - 配置Api和客户端
 - More...

## 安装使用
## 使用说明
### Prompt调试矩阵
### Prompt档案馆
### 向量数据库数据管理
### 文档灌库工具
### FunctionCalling调试

## 参与开发
